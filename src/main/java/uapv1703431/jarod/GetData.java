package uapv1703431.jarod;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

class GetData extends AsyncTask <String,Void,String> {

    City city;
    CityActivity activity;

    GetData (City _city, CityActivity _activity){
        this.activity = _activity;
        this.city = _city;
    }
    @Override
    protected String doInBackground(String... a) {
        Log.d("url", a[0]);
        HttpURLConnection connection;
        InputStream is;
        is = null;
        try {
            URL url = new URL(a[0]);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.connect();
            is = connection.getInputStream();
            Log.d("result:", is.toString());
            JSONResponseHandler Jresp = new JSONResponseHandler(this.city);
            Jresp.readJsonStream(is);

        } catch (IOException e) {
            e.printStackTrace();
            e.printStackTrace();
            e.printStackTrace();
        }


        return is.toString();
    }

    @Override
    protected void onPostExecute(String s) {
        activity.updateView();

    }
}